import SimpleOpenNI.*;

SimpleOpenNI  kinect;
color[]       userClr = new color[] { 
  color(255, 100, 0), 
  color(0, 255, 100), 
  color(0, 0, 255), 
  color(255, 255, 0), 
  color(255, 0, 255), 
  color(0, 255, 255)
};
PVector com = new PVector();                                   
PVector com2d = new PVector();                                   
PrintWriter file;
String outputStr;
int frame;
boolean nowRecording;

void setup()
{
  size(800, 600);

  frame = 0;
  nowRecording = false;

  // Kinectのセットアップ
  kinect = new SimpleOpenNI(this);
  if (kinect.isInit() == false)
  {
    println("Can't init SimpleOpenNI, maybe the camera is not connected!"); 
    exit();
    return;
  }

  // 深度画像を有効化
  kinect.enableDepth();

  // ユーザトラッキングを有効化
  kinect.enableUser();

  background(200, 0, 0);

  stroke(0, 0, 255);
  strokeWeight(1);
  smooth();
}

void draw()
{
  // Kiectのアップデート
  kinect.update();

  // イメージマップを書く
  image(kinect.userImage(), 0, 0);

  // ユーザートラッキング中なら
  int[] userList = kinect.getUsers();
  for (int i=0; i<userList.length; i++)
  { 
    // ユーザーのスケルトンを検出できていたら
    if (kinect.isTrackingSkeleton(userList[i]))
    {
      stroke(userClr[ (userList[i] - 1) % userClr.length ] );

      // スケルトンを描画
      drawSkeleton(userList[i]);

      // 録画中なら、座標取得
      if (nowRecording)
      {
        saveFrameOfSkelton(userList[i]);
      }
    }
  }
}

// 骨格の描画
void drawSkeleton(int userId)
{
  // 3D のジョイントデータ取得  
  PVector jointPos = new PVector();
  kinect.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_NECK, jointPos);

  // 2Dに変換
  PVector jointPos2D = new PVector(); 
  kinect.convertRealWorldToProjective(jointPos, jointPos2D);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);

  kinect.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
  kinect.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
}

// 骨格をファイルに書出し
void saveFrameOfSkelton(int userId)
{
  // フレーム数インクリメント
  frame ++;

  outputStr += getPosString(userId, SimpleOpenNI.SKEL_HEAD);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_ELBOW);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_HAND);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_HAND);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_TORSO);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_HIP);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_HIP);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_KNEE);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_KNEE);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_LEFT_FOOT);
  outputStr += getPosString(userId, SimpleOpenNI.SKEL_RIGHT_FOOT);
  outputStr += "-\n";
}

// 座標を得る
String getPosString(int userId, int posId)
{
  PVector jointPos = new PVector();
  kinect.getJointPositionSkeleton(userId, posId, jointPos);

  PVector jointPos2D = new PVector();
  kinect.convertRealWorldToProjective(jointPos, jointPos2D);
  // ellipse( jointPos2D.x, jointPos2D.y, 50, 50 );

  // フレーム数、posIDを加えて文字列として返す
  String str = frame + "," + posId + "," + jointPos2D.x + "," + jointPos2D.y + "," + jointPos2D.z + "\n";
  return str;
}

void saveToFile(String str)
{
  String fileName =  hour() + "_" + minute() + "_" + second();
  file = createWriter("/Users/fumiki/Desktop/" + fileName + ".csv"); 

  // 書き込み
  file.println(str);

  // 終了処理†
  file.flush();
  file.close();
}

// 録画を開始・停止
void toggleRecording() 
{
  // レコーディング開始時の処理
  if (!nowRecording)
  {
    println("- - - - - - - - - - - - - - - \n 録画を開始します ");
    
  }
  else
  {
    // レコーディング終了時の処理
    println("録画を停止しました : 計" + frame + "フレーム　\n- - - - - - - - - - - - - - -  ");
    // ファイルに書出し
    saveToFile(outputStr);
  }
  
  nowRecording = !nowRecording;
  frame = 0;
  outputStr = "";
}




// -----------------------------------------------------------------
// SimpleOpenNI events

// ユーザー検出時
void onNewUser(SimpleOpenNI curContext, int userId)
{
  println("onNewUser - userId: " + userId);
  println("\tstart tracking skeleton");

  curContext.startTrackingSkeleton(userId);
}

// ユーザー喪失時
void onLostUser(SimpleOpenNI curContext, int userId)
{
  println("onLostUser - userId: " + userId);
}

void onVisibleUser(SimpleOpenNI curContext, int userId)
{
  //println("onVisibleUser - userId: " + userId);
}

// キーボード操作
void keyPressed()
{
  switch(keyCode)
  {
  case ENTER :
    toggleRecording();
    break;

  case LEFT :
    kinect.setMirror(!kinect.mirror());  // ミラー反転を有効化
    break;
  }
}  

